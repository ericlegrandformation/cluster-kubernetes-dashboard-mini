# Cluster KUBERNATES - VirtualBox

![a](./images/LogoKubernates.png?zoomresize=480%2C240)

Ce projet va vous permettre de :
 * d'utiliser Vagrant pour provisionner l'infrastructure dans VirtualBox
 * d'utiliser Ansible pour installer et configurer KUBERNATES


## Pour commencer

### Pré-requis

Ce qui est requis pour commencer avec ce projet...

- Un ordinateur connecté à internet préférablement avec Linux
- des connaissances de base en système Linux, réseaux informatique et applications Web
- des notions de programmation (scripting, Python, PHP, ..)
- avoir déjà utilisé Git, VirtualBox et un outil de Dev (par exemple VSCode) seront un plus utile

## Installation

Nous allons installer un cluster KUBERNATES à l'aide de Vagrant et Ansible.

Ci dessous, l'architecture complète de notre projet.
Pour simplifier l'exercice, il n'y aura qu'un seul master et un seul worker.
Et nous allons nous servir principalement du réseau 19.168.50. pour installer et communiquer avec les machines du cluster Kubernates. 

![a](./images/Kubernates.png?zoomresize=480%2C240)

C'est parti !

Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, 

* dans un terminal, taper :
``` 
$ git clone https://framagit.org/ericlegrandformation/cluster-kubernates-stand-alone.git 
```
* ouvrer le dossier "cluster-kubernates-stand-alone" **avec VSCode**

* ouvrer un **nouveau terminal** dans VSCode

* créer un environnement virtuel python avec la commande
 ```
$ virtualenv --python=python3.5 venv
 ```
* activer l'environnement virtuel python avec la commande
 ```
$ source ./venv/bin/activate
  ```
* installer quelques pré-requis avec la commande
 ```
$ sudo apt install libffi-dev python-dev libssl-dev
  ```
* installer ansible avec la commande
 ```
$ pip install ansible
  ```
* vérifier la version ansible avec
 ```
$ ansible --version (version 2.8 mini)
  ```
* Lancer la création des VM VirtualBox avec la commande
 ```
$ vagrant up
  ```
* Configurer les VM master-1 et worker-1 avec la commande
 ```
$ ansible-playbook roles/main.yml
  ```
* Vérifier l'installation du master et du worker Kubernates avec la commande
 ```
$ ssh vagrant@192.168.50.11
  taper le mot de passe "vagrant" et une fois connecté:
$ kubectl get nodes
  si tout va bien, lire:
NAME       STATUS   ROLES    AGE     VERSION
master-1   Ready    master   5d10h   v1.16.2
worker-1   Ready    <none>   5d10h   v1.16.2 
  ```
  
*Bravo, vous venez d'installer KUBERNATES.*

***

## Versions

**Dernière version stable :** 0.1
**Dernière version :** 0.1
Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/cluster-kubernates-stand-alone.git)

## Auteurs

* **Eric Legrand** _alias_ [@ericlegrand](https://framagit.org/ericlegrandformation/cluster-kubernates-stand-alone.git)